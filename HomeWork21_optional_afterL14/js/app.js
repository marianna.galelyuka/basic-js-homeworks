// Завдання
// Намалювати на сторінці коло за допомогою параметрів, які введе користувач. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
// При натисканні на кнопку "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При кліку на конкретне коло - це коло має зникати, у своїй порожнє місце заповнюватися, тобто інші кола зрушуються вліво.
// У вас може виникнути бажання поставити обробник події на кожне коло для його зникнення. Це неефективно, так не треба робити. На всю сторінку має бути лише один обробник подій, який це робитиме.


const startBtn = document.querySelector('button');

const diametrLabel = document.createElement('label');
diametrLabel.textContent = 'Введіть діаметр кола';
diametrLabel.style.display = 'none';
document.body.insertAdjacentElement('beforeend', diametrLabel);

const diametrInput = document.createElement('input');
diametrInput.setAttribute('type', 'number');
diametrInput.style.display = 'none';
document.body.insertAdjacentElement('beforeend', diametrInput);

const drawBtn = document.createElement('button');
drawBtn.textContent = 'Намалювати';
drawBtn.style.display = 'none';
document.body.insertAdjacentElement('beforeend', drawBtn);

const div = document.createElement('div');
document.body.insertAdjacentElement('beforeend', div);

startBtn.addEventListener('click', (ev) => {
    diametrLabel.style.display = 'block';
    diametrInput.style.display = 'block';
    drawBtn.style.display = 'block';
});

drawBtn.addEventListener('click', drawCircles);
function drawCircles(ev) {
    const diametr = parseInt(document.querySelector('input').value);
    const radius = diametr / 2;

    div.style.display = 'grid';
    div.style.gridTemplateColumns = '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr';
    div.style.gridTemplateRows = '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr';

    for (let i = 1; i <= 100; i++) {
        const canvas = document.createElement('canvas');
        canvas.setAttribute('width', diametr);
        canvas.setAttribute('height', diametr);
        
        const ctx = canvas.getContext('2d');
        const pi = Math.PI;
        let coordinateX = radius;
        let coordinateY = radius;

        ctx.beginPath();
        ctx.fillStyle = chooseColor();
        ctx.arc(coordinateX, coordinateY, radius, 0, 2*pi, false);
        ctx.fill();
        div.append(canvas);
    }
};

function chooseColor() {
    let color1 = (Math.round(Math.random()*255));
    let color2 = (Math.round(Math.random()*255));
    let color3 = (Math.round(Math.random()*255));

    return(`rgb(${color1}, ${color2}, ${color3})`);
};

div.addEventListener('click', deleteCircle);
function deleteCircle(ev) {
    ev.target.remove()
};