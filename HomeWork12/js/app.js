// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


const btns = document.querySelectorAll('.btn');
console.log(btns);
// or
// const btns = Array.from(document.body.getElementsByClassName('btn'));

// function changeBackColor(arr) {
//     document.addEventListener('keyup', (ev) => {
//         arr.forEach(el => {
//             if(ev.key.toUpperCase() === el.dataset.letter.toUpperCase()) {
//                 el.style.backgroundColor = '#0000ff';
//             } else {
//                 el.style.backgroundColor = '#000000';
//             }
//         })
//     })
// };
// changeBackColor(btns);


    // код для будь-якої розкладки
document.addEventListener('keyup', changeColor);

function changeColor(ev) {
    btns.forEach(el => {
        if(ev.code.toLowerCase() === `key${el.dataset.letter.toLowerCase()}` || ev.key.toLowerCase() === `${el.dataset.letter.toLowerCase()}`) {
            el.style.backgroundColor = '#0000ff';
        } else {
            el.style.backgroundColor = '#000000';
        }
    })
};