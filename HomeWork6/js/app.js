// Завдання
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.


function createNewUser() {
    let newUser = {
        firstName: prompt('Enter your name, please'),
        lastName: prompt('Enter your last name, please'),
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        birthday: prompt('Enter your date of birth, please - dd.mm.yyyy'),
        getAge: function() {
            let today = new Date();
            let age;
            // return (today.getFullYear() - this.birthday.slice(-4)); 
            if (today.getMonth() + 1 == this.birthday.slice(3, 5)) {
                if (today.getDate() >= this.birthday.slice(0, 2)) {
                    age = (today.getFullYear() - this.birthday.substr(-4, 4));
                } else {
                    age = (today.getFullYear() - this.birthday.substr(-4, 4) - 1);
                };
            } else if (today.getMonth() + 1 > this.birthday.slice(3, 5)) {
                age = (today.getFullYear() - this.birthday.substr(-4, 4));
            } else {
                age = (today.getFullYear() - this.birthday.substr(-4, 4) - 1);
            };
            return age;
        },
        getPassword: function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(this.birthday.length - 4);
            // return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
            // return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(-4, 4);
        },
    }
    return newUser;
}
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
