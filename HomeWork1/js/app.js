// 1.	Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
const name = 'Marianna';
const admin = name;
console.log(admin);

//variant_2
// let admin, name;
// name = 'Marianna';
// admin = name;
// console.log(admin);


// 2.	Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
const days = 5;
const seconds = days * 24 * 60 * 60;
console.log(seconds); 


// 3.	Запитайте у користувача якесь значення і виведіть його в консоль.
const age = prompt('How old are you?');
console.log(parseInt(age));
