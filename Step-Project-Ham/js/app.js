    // Вкладки у секції Our services повинні перемикатися при натисканні мишею. Текст та картинки для інших вкладок додати будь-які.
const servicesList = document.querySelector('.services-list');
const servicesItems = document.querySelectorAll('.service-item');
const servicesDescription = document.querySelectorAll('.service-item-description');

servicesList.addEventListener('click', isActiveTab);
function isActiveTab(ev) {
    servicesItems.forEach(item => {
        item.classList.remove('service-item-active')
    })
    ev.target.classList.add('service-item-active');
    servicesDescription.forEach(el => {
        el.getAttribute('id') === ev.target.dataset.tab ? el.classList.add('service-description-active') : el.classList.remove('service-description-active');
    })
}


    // Кнопка Load more у секції Our amazing work імітує завантаження з сервера нових картинок. При її натисканні в секції знизу мають з'явитись ще 12 картинок (зображення можна взяти будь-які). Після цього кнопка зникає.
    // Кнопку Load more у секції Our amazing work можна натиснути двічі, кожне натискання додає 12 картинок знизу. Тобто максимум у цій секції може бути 36 картинок. Після другого натискання кнопка зникає.
    // При натисканні на кожну з кнопок Load more імітувати завантаження картинок із сервера. Показувати замість кнопки або над нею дві секунди CSS анімацію завантаження (можна написати самому або взяти будь-який приклад з інтернету, наприклад тут або тут), і лише після цього додавати картинки на сторінку.

// const loadMoreWorksBtn = document.querySelector('.works-more-btn');
// const worksExamples = Array.from(document.querySelector('.works-examples').children);
// const worksHidden = document.querySelector('.works-examples-hidden');
// const loading = document.querySelector('.loading');

// loadMoreWorksBtn.addEventListener('click', loadMoreWorks);
// function loadMoreWorks(ev) {
//     loadMoreWorksBtn.style.display = 'none';
//     loading.style.display = 'flex';
//     setTimeout(function() {
//         if (worksExamples[1].classList.contains('works-examples-hidden')) {
//             worksExamples[1].classList.remove('works-examples-hidden');
//             loadMoreWorksBtn.style.display = 'initial';
//             loading.style.display = 'none';
//         } else {
//             worksExamples[2].classList.remove('works-examples-hidden');
//             loadMoreWorksBtn.style.display = 'none';
//             loading.style.display = 'none';
//         }
//     }, 2000)
// }

const worksExamplesImg = document.querySelectorAll('.works-examples-images');
const loadMoreWorksBtn = document.querySelector('.works-more-btn');
const loading = document.querySelector('.loading');

loadMoreWorksBtn.addEventListener('click', loadMoreWorks);
function loadMoreWorks(ev) {
    const worksHidden = document.querySelectorAll('.works-examples-hidden');
    const activeTab = document.querySelector('.works-item-active');
    
    loadMoreWorksBtn.style.display = 'none';
    loading.style.display = 'flex';

    setTimeout(function() {
        if (worksHidden.length > 12) {
            for (let i = 0; i < 12; i++) {
                worksHidden[i].classList.remove('works-examples-hidden');
                for (let j = 0; j < 24; j++) {
                    if (activeTab.dataset.type === 'all') {
                        worksExamplesImg[j].style.display = 'grid';
                    } else {
                        if (worksExamplesImg[j].getAttribute('id') !== activeTab.dataset.type) {
                            worksExamplesImg[j].style.display = 'none';
                        }
                    }
                }
                loadMoreWorksBtn.style.display = 'initial';
                loading.style.display = 'none';
            }
        } else {
            worksHidden.forEach(item => {
                item.classList.remove('works-examples-hidden');
            });
            worksExamplesImg.forEach(el => {
                if (activeTab.dataset.type === 'all') {
                    el.style.display = 'grid';
                } else {
                    if (el.getAttribute('id') !== activeTab.dataset.type) {
                        el.style.display = 'none';
                    }
                }
            })
            loadMoreWorksBtn.style.display = 'none';
            loading.style.display = 'none';
        }
    }, 2000)
}


    // Кнопки на вкладці Our amazing work є "фільтрами продукції". Попередньо кожній із картинок потрібно присвоїти одну з чотирьох категорій, на ваш розсуд (на макеті це Graphic design, Web design, Landing pages, Wordpress).При натисканні на кнопку категорії необхідно показати лише ті картинки, які належать до цієї категорії. All показує картинки з усіх категорій. Категорії можна перейменувати, картинки для категорій взяти будь-які.
const worksMenu = document.querySelector('.works-menu-list');
const worksMenuItems = document.querySelectorAll('.works-item');

worksMenu.addEventListener('click', filterWorksExample);
function filterWorksExample(ev) {
    if (ev.target.tagName.toLowerCase() === 'li') {
        worksMenuItems.forEach(item => {
            item.classList.remove('works-item-active')
        })
        ev.target.classList.add('works-item-active');

        worksExamplesImg.forEach(el => {
            if(!el.classList.contains('works-examples-hidden')) {
                el.style.display = 'grid';
                
                if (ev.target.dataset.type === 'all') {
                    el.style.display = 'grid';
                } else {
                    if (el.getAttribute('id') !== ev.target.dataset.type) {
                        el.style.display = 'none';
                    }
                }
            }

        })
    }
}

    // Карусель на вкладці What people say about theHam має бути робочою, по кліку як на іконку фотографії внизу, так і на стрілки вправо-вліво. У каруселі має змінюватися як картинка, і текст. Карусель обов'язково має бути з анімацією.

    // карусель по кліку на іконку фотографії
const feedbackAuthorsSlider = document.querySelector('.feedback-authors');
const feedbackAuthorsAll = Array.from(feedbackAuthorsSlider.children);
const feedbackContents = document.querySelectorAll('.feedback-content');

feedbackAuthorsSlider.addEventListener('click', chooseFeedbackAuthor);
function chooseFeedbackAuthor(ev) {
    feedbackAuthorsAll.forEach(el => {
        el.classList.remove('author-photo-active')
    })
    if (ev.target.tagName.toLowerCase() === 'img') {
        ev.target.parentElement.classList.add('author-photo-active');
        ev.target.previousElementSibling.animate({
            transform: 'scale(1.3)',
            borderRadius: '10%',
        }, 500)
        ev.target.previousElementSibling.animate({
            transform: 'scale(1)',
            borderRadius: '50%',
        }, 500)
    }

    feedbackContents.forEach(el => {
        el.classList.remove('feedback-active')
    })
    feedbackContents.forEach(item => {
        item.getAttribute('id') === ev.target.dataset.name ? item.classList.add('feedback-active') : item.classList.remove('feedback-active');
    })
}

    // карусель по кліку на стрілку вліво-вправо
const sliderBtns = document.querySelectorAll('.slider-button');
let currentIndex;
let previousIndex;
let nextIndex;

sliderBtns.forEach(btn => {
    btn.addEventListener('click', showAnotherFeedback);
});

function showAnotherFeedback(ev) {
    for (let i = 0; i < feedbackAuthorsAll.length; i++) {
        if (feedbackAuthorsAll[i].classList.contains('author-photo-active')) {
            currentIndex = i;
        }
    }
    feedbackAuthorsAll[currentIndex].classList.remove('author-photo-active');
    feedbackContents[currentIndex].classList.remove('feedback-active');

    if (ev.target.dataset.side === 'left') {
        showPreviousFeedback();
    }

    if (ev.target.dataset.side === 'right') {
        showNextFeedback();
    }
}

function showPreviousFeedback() {    
    if (currentIndex >= 1) {
        previousIndex = currentIndex - 1
    } 
    if (currentIndex === 0) {
        previousIndex = feedbackAuthorsAll.length - 1
    }

    feedbackAuthorsAll[previousIndex].classList.add('author-photo-active');
    feedbackAuthorsAll[previousIndex].animate({
        transform: 'rotate(-360deg)',
    }, 500)

    feedbackContents[previousIndex].classList.add('feedback-active');
}

function showNextFeedback() {
    if (currentIndex < feedbackAuthorsAll.length - 1) {
        nextIndex = currentIndex + 1
    } 
    if (currentIndex === feedbackAuthorsAll.length - 1) {
        nextIndex = 0
    }

    feedbackAuthorsAll[nextIndex].classList.add('author-photo-active');
    feedbackAuthorsAll[nextIndex].animate({
        transform: 'rotate(360deg)',
    }, 500)

    feedbackContents[nextIndex].classList.add('feedback-active');
}



    // Кнопка Load more у секції Gallery of best images також має бути робочою та додавати порцію нових картинок на сторінку.
const loadMoreImagesBtn = document.querySelector('.gallery-more-btn');
const imagesHidden = document.querySelectorAll('.images-hidden');
const loadingImg = document.querySelector('.gallery-loading-img');

loadMoreImagesBtn.addEventListener('click', loadMoreImages);
function loadMoreImages(ev) {
    loadMoreImagesBtn.style.display = 'none';
    loadingImg.style.display = 'flex';
    setTimeout(function() {
        imagesHidden.forEach(el => {
            el.classList.remove('images-hidden');
            loadingImg.style.display = 'none';
        })
    }, 2000)
}
