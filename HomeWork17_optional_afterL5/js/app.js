// Завдання
// Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити порожній об'єкт student, з полями name та lastName.
// Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
// порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.


        // variant 1
let student = {
    name: prompt('Enter your name, please'),
    lastName: prompt('Enter your last name, please'),
};

student.tabel = {};
let key = prompt('Enter the subject, please');
let value;
while (key !== null) {
    value = +prompt('Enter your rating on this subject');
    student.tabel[key] = value;
    key = prompt('Enter the subject, please');
};

const studentTabel = Object.values(student.tabel);
let count = 0;
let number = 0;
let total = 0;
for (let i = 0;  i < studentTabel.length; i++) {
    total += studentTabel[i];
    number += 1;
    if(studentTabel[i] < 4) {
        count += 1;
    };
}
let average = (total / number);
if (count === 0 && average > 7) {
    console.log('Студент переведено на наступний курс. Студенту призначено стипендію');
} else if (count === 0 && average <= 7) {
    console.log('Студент переведено на наступний курс');
} else {
    console.log('Студент не переведено на наступний курс');
};


        // variant 2 - with functions
// function getName(){
//     return (prompt('Enter your name, please'));
// };
// function getLastName() {
//     return (prompt('Enter your last name, please'));
// };
// function createTabel() {
//     let tabel = {};
//     let key = prompt('Enter the subject, please');
//     let value;
//     while (key !== null) {
//         value = +prompt('Enter your rating on this subject');
//         tabel[key] = value;
//         key = prompt('Enter the subject, please');
//     };
//     return tabel;
// };
// function createStudent(name, lastName, tabel) {
//     return {
//         name: name,
//         lastName: lastName,
//         tabel: tabel,
//     }
// }
// let student = createStudent(getName(), getLastName(), createTabel());
// console.log(student);

// function checkTabel(tabel) {
//     let count = 0;
//     let number = 0;
//     let total = 0;
//     for (let i = 0;  i < studentTabel.length; i++) {
//         total += studentTabel[i];
//         number += 1;
//         if(studentTabel[i] < 4) {
//             count += 1;
//         };
//     }
//     let average = (total / number);
//     if (count === 0 && average > 7) {
//         return ('Студент переведено на наступний курс. Студенту призначено стипендію');
//     } else if (count === 0 && average <= 7) {
//         return ('Студент переведено на наступний курс');
//     } else {
//         return ('Студент не переведено на наступний курс');
//     };
// };
// let studentTabel = Object.values(student.tabel);
// console.log(checkTabel(studentTabel));