// Завдання
// Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

// Необов'язкове завдання підвищеної складності
// При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
// Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.


const imagesWrapper = document.querySelector('div');
const images = document.querySelectorAll('img');

const startBtn = document.querySelector('.start');

const stopBtn = document.querySelector('.stop');
stopBtn.style.display = 'none';

const resumeBtn = document.querySelector('.resume');
resumeBtn.style.display = 'none';

let timerId;
let currentImageIndex;
let nextImageIndex;

        // setInterval
// startBtn.addEventListener('click', (ev) => {
//     currentImageIndex = 0;
//     images[currentImageIndex].classList.add('active');
//     timerId = setInterval(showNextImage, 3000);
//     startBtn.setAttribute('disabled', true);
//     stopBtn.style.display = 'flex';
//     resumeBtn.style.display = 'flex';
// })

// function showNextImage() {
//     for (let i = 0; i < images.length; i++) {
//         if(images[i].classList.contains('active')) {
//             currentImageIndex = i;
//         }
//     }

//     if (currentImageIndex >= 0 && currentImageIndex < images.length - 1) {
//         nextImageIndex = currentImageIndex + 1;
//     } else {
//         nextImageIndex = 0;
//     }

//     images[currentImageIndex].classList.remove('active');
//     images[nextImageIndex].classList.add('active');
// }

// stopBtn.addEventListener('click', (ev) => {
//     clearInterval(timerId);
//     stopBtn.setAttribute('disabled', true);
// })

// resumeBtn.addEventListener('click', (ev) => {
//     timerId = setInterval(showNextImage, 3000);
//     stopBtn.removeAttribute('disabled', true);
// })


      //  рекурсивний setTimeout
startBtn.addEventListener('click', (ev) => {
    currentImageIndex = 0;
    images[currentImageIndex].classList.add('active');
    timerId = setTimeout(showNextImage, 3000);
    startBtn.setAttribute('disabled', true);
    stopBtn.style.display = 'flex';
    resumeBtn.style.display = 'flex';
})

function showNextImage() {
    for (let i = 0; i < images.length; i++) {
        if(images[i].classList.contains('active')) {
            currentImageIndex = i;
        }
    }

    if (currentImageIndex >= 0 && currentImageIndex < images.length - 1) {
        nextImageIndex = currentImageIndex + 1;
    } else {
        nextImageIndex = 0;
    }

    images[currentImageIndex].classList.remove('active');
    images[nextImageIndex].classList.add('active');

    timerId = setTimeout(showNextImage, 3000)
}

stopBtn.addEventListener('click', (ev) => {
    clearTimeout(timerId);
    stopBtn.setAttribute('disabled', true);
})

resumeBtn.addEventListener('click', (ev) => {
    timerId = setTimeout(showNextImage, 3000);
    stopBtn.removeAttribute('disabled', true);
})
