// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.


    // variant 1
function isActiveTab () {
    const tabsTitles = Array.from(document.querySelectorAll('.tabs-title'));
    // tabsTitles.forEach((title) => {
    //     title.setAttribute('data-tab', `${title.innerText}`);        // if not to add 'data-tab' in html
    // });

    const tabsContent = document.querySelector('.tabs-content');
    const tabsContentItems = Array.from(tabsContent.children);

    const tabs = document.querySelector('.tabs');
    tabs.addEventListener('click', (ev) => {
        tabsTitles.forEach(title => {
            title.classList.remove('active')
        })
        ev.target.classList.add('active');
        tabsContentItems.forEach(item => {
            item.getAttribute('id') === ev.target.dataset.tab ? item.classList.add('active') : item.classList.remove('active');
        })
    })
};
isActiveTab();
    

    // variant 2
// let tabName;

// const tabsTitles = Array.from(document.querySelectorAll('.tabs-title'));
// tabsTitles.forEach((title) => {
//     title.addEventListener('click', selectTab)
// });

// const tabsContent = document.querySelector('.tabs-content');
// const tabsContText = Array.from(tabsContent.children);

// function selectTab() {
//     tabsTitles.forEach(item => {
//         item.classList.remove('active')
//     });
//     this.classList.add('active');
//     tabName = this.getAttribute('data-tab');
//     selectTabContent(tabName);
// }

// function selectTabContent(tabName) {
//     tabsContText.forEach(item => {
//         item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active')
//     })
// }
    

    // variant 3
// const tabsTitles = Array.from(document.querySelectorAll('.tabs-title'));

// const tabsContent = document.querySelector('.tabs-content');
// const tabsContText = Array.from(tabsContent.children);

// function clearActiveClass (array) {
//     array.find(el => el.classList.remove('active'));
// };

// function setActiveClass (array, index) {
//     array[index].classList.add('active');
// };

// function checkoutTab (el, index) {
//     el.addEventListener('click', () => {
//         clearActiveClass(tabsTitles);
//         clearActiveClass(tabsContText);
        
//         setActiveClass(tabsTitles, index);
//         setActiveClass(tabsContText, index);
//     })
// }
// tabsTitles.forEach(checkoutTab);