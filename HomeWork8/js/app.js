// Завдання
// Код для завдань лежить в папці project.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи, вкладені в елемент із класом main-header, і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const allParagraphs = document.body.querySelectorAll('p');
// або
// const allParagraphs = Array.from(document.getElementsByTagName('p'));
allParagraphs.forEach(element => element.style.backgroundColor = '#ff0000');
// або
// allParagraphs.forEach(changeBckgrColor);
// function changeBckgrColor(element) {
//     element.style.backgroundColor = '#ff0000';
// }

const optionsList = document.getElementById('optionsList');
// або
// const optionsList = document.querySelector('#optionsList');
console.log(optionsList);
const parent = optionsList.parentElement;
console.log(parent);
const optionsListChildren = Array.from(optionsList.childNodes);
optionsListChildren.forEach(item => console.log(item.nodeName, item.nodeType));
// або
// for (let i = 0; i < optionsListChildren.length; i++) {
//     console.log(optionsListChildren[i].nodeName, optionsListChildren[i].nodeType);
// }

const testParagraph = document.body.querySelector('#testParagraph');
// або
// const testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);
testParagraph.innerText = 'This is a paragraph';

const mainHeaderElements = document.body.querySelector('.main-header').querySelectorAll('*');
console.log(mainHeaderElements);
mainHeaderElements.forEach(element => element.classList.add('nav-item'));                   // якщо потрібно до існуючих класів додати клас nav-item
// mainHeaderElements.forEach(element => element.setAttribute('class', 'nav-item'));        // якщо потрібно замінити існуючі класи на клас nav-item

const sectionTitleElements = document.body.querySelectorAll('.section-title');
console.log(sectionTitleElements);
sectionTitleElements.forEach(element => element.classList.remove('section-title'));
