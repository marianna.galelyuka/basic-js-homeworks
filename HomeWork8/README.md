**HW8 - searching-dom-tasks**


**1. Опишіть своїми словами що таке Document Object Model (DOM).**
DOM - це структура документа, яка представляє вміст сторінки як об'єкти, якими можна маніпулювати через JS.

**2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?**
За допомогою елемента innerHTML через JavScript можна додати на сторінку тег з текстом, innerText додає на сторінку тільки текст і не розпізнає теги.

**3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?**
До елемента сторінки за допомогою JS можна звернутись наступним чином: для тегів head, body - document.head, document.body; для будь-якого елементу сторінки - з використанням спеціальних пошукових методів: getElementById(), getElementsByClassName(), getElementsByTagName(), querySelector(), querySelectorAll(); через дітей (document.element.childNodes, document.element.firstChild, document.element.lastChild), батьків (document.element.parentElement), сусідів (document.element.previousElementSibling, document.element.nextElementSibling).