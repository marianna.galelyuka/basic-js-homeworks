// Задание
// Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
// Считать с помощью модального окна браузера число, которое введет пользователь (n).
// С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
// Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).


const number1 = parseInt(prompt('Enter first number please'));
const number2 = parseInt(prompt('Enter second number please'));
const n = parseInt(prompt('Enter total quantity of numbers please'));

// function getFibonacci(F0, F1, n) {
//     let F = 0;

//     if (n === 0) {
//         F = F0;
//     }  else if ( n === 1) {
//         F = F1
//     } else if (n >= 2) {
//         for (let i = 2; i <= n; i++) {
//             F = F0 + F1;
//             F0 = F1;
//             F1 = F;
//         }
//     } else if (n < 0) {
//         for (let i = -1; i >= n; i--) {
//             F = F1 - F0;
//             F1 = F0;
//             F0 = F;
//         }
//     }
//     return F;
// };


        // рекурсія
function getFibonacci(F0, F1, n) {
    let F = 0;

    if (n === 0) {
        F = F0;
    }
    if (n === 1) {
        F = F1;
    } 
    if (n >= 2) {
        F = getFibonacci(F0, F1, (n - 1)) + getFibonacci(F0, F1, (n - 2));
    }
    if (n < 0) {
        F = getFibonacci(F0, F1, (n + 2)) - getFibonacci(F0, F1, (n + 1));
    }
    return F;
};

const fib = getFibonacci(number1, number2, n);
console.log(fib);


        // explanation for myself:
// F0 = 3;
// F1 = 7;
// F2 = 10;  =>  n = 2: getFibonacci(F0, F1)
// F3 = 17;  =>  n = 3: getFiconacci(F1; getFiconacci(n - 1));
// F4 = 27;  =>  n = 4: getFibonacci(getFibonacci(n - 2), getFibonacci(n - 1))
// F5 = 44;  =>  n = 5: getFibonacci(getFibonacci(n - 2), getFibonacci(n - 1))

