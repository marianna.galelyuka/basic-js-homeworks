// Завдання
// Реалізувати функцію, яка дозволить оцінити, чи команда розробників встигне здати проект до настання дедлайну. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Функція на вхід приймає три параметри:
// масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді. Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
// масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі. Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// Дата дедлайну (об'єкт типу Date).
// Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// Робота триває по 8 годин на день по будніх днях


function getStartDate() {
    let startDate = new Date();

    if (startDate.getDay() === 0) {
        startDate.setDate(startDate.getDate() + 1);
    } else if (startDate.getDay() === 6) {
        startDate.setDate(startDate.getDate() + 2);
    } else {
        startDate = startDate;
    }
    return startDate;
};

function getTotalTaskQuantity (arr) {
    let totalTaskQuantity = 0;
    arr.forEach(el => {
        totalTaskQuantity += el;
    })    
    return totalTaskQuantity;
};

function getSpeedPerDay (arr) {
    let totalSpeed = 0;
    arr.forEach(el => {
        totalSpeed += el;
    });
    return totalSpeed;
};

function checkDeadline(arrSpeed, arrBacklog, deadline) {
    const startDate = getStartDate();
    const availableTime = deadline.getTime() - startDate.getTime();
    const availableDays = availableTime / 1000 / 60 / 60 / 24;

    const totalTasks = getTotalTaskQuantity(arrBacklog);
    const totalSpeed = getSpeedPerDay(arrSpeed);
    const necessaryDays = totalTasks / totalSpeed;


    if (availableDays > necessaryDays) {
        inAdvance = Math.round(availableDays - necessaryDays);
        return `Усі завдання будуть успішно виконані за ${inAdvance} днів до настання дедлайну!`
    }
    if (availableDays < necessaryDays) {
        hoursAdditional = Math.round((necessaryDays - availableDays) * 8);
        return `Команді розробників доведеться витратити додатково ${hoursAdditional} годин після дедлайну`
    }
};

const speed = [5, 7, 6, 8];                 // кожен працівник за 1 день (8 год)
const taskQuantity = [75, 73, 68, 93]       // кількість завдань в кожному блоці
const deadline = new Date(2023, 1, 13);

const result = checkDeadline(speed, taskQuantity, deadline);
console.log(result);
