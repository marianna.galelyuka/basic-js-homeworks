// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.

// Необов'язкове завдання підвищеної складності:
    // Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:
    // ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
    // Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.


const ul = document.createElement('ul');
function createList(array, parent = document.body) {
    array.forEach((item) => {
        const li = document.createElement('li');
        if (!Array.isArray(item)) {
            li.textContent = item;
            ul.append(li);
        } 
        if (Array.isArray(item)) {
            const ulLevel2 = document.createElement('ul');
            for (let i = 0; i < item.length; i++) {
                const liLevel2 = document.createElement('li');
                liLevel2.textContent = item[i];
                ulLevel2.append(liLevel2);
                li.append(ulLevel2);
                ul.append(li);
            }
        }
    })
    parent.append(ul);
}
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// createList(array);
createList(arr);

// Очистити сторінку через 3 секунди:
// setTimeout (() => ul.remove(), 3000);

// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки:
function printNumbers(from) {
    const body = document.querySelector('body');
    const div = document.createElement('div');
    let current = from;
    let timerId = setTimeout(function go() {
        if (current >= 10) {
            div.textContent = `${current} sec`;
        } else {
            div.textContent = `0${current} sec`;
        };
        if (current > 0) {
            setTimeout(go, 1000);
        } else {
            // div.style.display = 'none';
            clearTimeout(timerId);
            ul.remove()
        };
        current--;
    }, 0);
    body.append(div);
  }
  printNumbers(3);