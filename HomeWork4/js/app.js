// Завдання
// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).


let a = +prompt('Enter number one');
let b = +prompt('Enter number two');
let operation = prompt('Choose the operation for the numbers: +, -, *, /');

while (Number.isNaN(a) || Number.isNaN(b)) {
    a = +prompt('Enter number one', a);
    b = +prompt('Enter number two', b);
};

    // variant 1
// function showResult(a, b, operation) {
//     let result;
//     if (operation === '+') {
//         result = a + b;
//     } else if (operation === '-') {
//         result = a - b;
//     } else if (operation === '*') {
//         result = a * b;
//     } else if (operation === '/') {
//         result = a / b;
//     }
//     console.log(result);
// };

// showResult(a, b, operation);

    
    // variant 2
function showResult (a, b, operation) {
    if (operation === '+') {
        return a + b;
    } else if (operation === '-') {
        return a - b;
    } else if (operation === '*') {
        return a * b;
    } else if (operation === '/') {
        return a / b;
    }
};

// console.log(showResult(a, b, operation));
let result = showResult(a, b, operation);
console.log(result);


    // variant 3
// let result = function(a, b, operation) {
//     if (operation === '+') {
//         return a + b;
//     } else if (operation === '-') {
//         return a - b;
//     } else if (operation === '*') {
//         return a * b;
//     } else if (operation === '/') {
//         return a / b;
//     }
// };

// console.log(result(a, b, operation));