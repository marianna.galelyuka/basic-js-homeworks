// Завдання
// Реалізувати можливість зміни колірної теми користувача. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки


const menuItems = document.querySelectorAll('a');
const startBtn = document.querySelector('.start-btn');

const changeBtn = document.createElement('button');
changeBtn.textContent = 'Змінити тему';
document.body.insertAdjacentElement('afterbegin', changeBtn)

changeBtn.addEventListener('click', changeColor);
function changeColor() {
    if (localStorage.getItem('btnColor') !== null || localStorage.getItem('itemColor') !== null) {
        localStorage.clear();
        startBtn.style.background = '';
        menuItems.forEach(item => {
            item.style.color = ''
        })
    } else {
        startBtn.style.background = '#003b09';
        localStorage.setItem('btnColor', '#003b09');
        menuItems.forEach(item => {
            item.style.color = 'darkblue'
        })
        localStorage.setItem('itemColor', 'darkblue');
    }
}

window.onload = function() {
    if (localStorage.getItem('btnColor') !== null || localStorage.getItem('itemColor') !== null) {
        let buttonColor = localStorage.getItem('btnColor');
        let menuColor = localStorage.getItem('itemColor');
        startBtn.style.background = buttonColor;
        menuItems.forEach(item => {
            item.style.color = menuColor;
        })
    }
}
