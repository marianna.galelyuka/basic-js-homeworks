// Завдання
// Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// Використовувати синтаксис ES6 для роботи зі змінними та функціями.
// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, або при введенні вказав не число, - запитати число заново (при цьому значенням для нього повинна бути введена раніше інформація).

        // ітеративний спосіб
// let number = parseInt(prompt('Enter a number you like!'));
// while (Number.isNaN(number)) {
//     number = parseInt(prompt('Enter a number you like!', number));
// };

// function factorial(n) {
//     let result = 1;
//     if (n === 0) {
//         result = 1;
//     } else {
//         for (let i = 1; i <= n; i++) {
//             result *= i;
//         }
//     }
//     return result;
// }

// const product = factorial(number);
// console.log(product);


        // рекурсія
function getNumber() {
    let number = parseInt(prompt('Enter a number you like!'));
    if (Number.isNaN(number)) {
        return getNumber();
    };
    return number;
}

function factorial(n) {
    if (n === 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

const number = getNumber();
const product = factorial(number);
console.log(product);


        // рекурсія + тернарний оператор
// function getNumber() {
//     let number = parseInt(prompt('Enter a number you like!'));
//     return (Number.isNaN(number)) ? getNumber() : number;
// }

// function factorial(n) {
//     return (n === 0) ? 1 : n * factorial(n - 1);
// }

// const number = getNumber();
// const product = factorial(number);
// console.log(product);